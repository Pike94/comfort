'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

$('input[type="tel"]').inputmask();

$('.use-slider').slick({
  arrows: true,
  dots: true,
});

$(document).on('click', '.js-add-items', function(){
  $('.calc-table-body').append('<tr class="calc-table-body-row">' +
    '<td class="calc-table-body-item">' +
      '<div class="calc-table-body-item-row">' +
        '<button type="button" class="js-remove-btn remove-btn"></button>' +
        '<select name="object" class="select select-object">' +
          '<option value="Кухня">Кухня</option>' +
          '<option value="Зала">Зала</option>' +
          '<option value="Інше">Інше</option>' +
        '</select>' +
      '</div>' +
    '</td>' +
    '<td class="calc-table-body-item">' +
      '<select name="place" class="select select-place">' +
        '<option value="Під плитку">Під плитку</option>' +
        '<option value="Під ламінат">Під ламінат</option>' +
        '<option value="Під ковролін">Під ковролін</option>' +
        '<option value="Під лінелеум">Під лінелеум</option>' +
        '<option value="Під стяжку">Під стяжку</option>' +
      '</select>' +
    '</td>' +
    '<td class="calc-table-body-item">' +
      '<select name="type" class="select select-type">' +
        '<option value="Мат 160">Мат 160</option>' +
        '<option value="Мат 80">Мат 80</option>' +
        '<option value="Кабель 18">Кабель 18</option>' +
      '</select>' +
    '</td>' +
    '<td class="calc-table-body-item">' +
      '<select name="termo" class="select select-termo">' +
        '<option value="C101 - електронний терморегулятор">C101 - електронний терморегулятор</option>' +
        '<option value="C501 - електронний терморегулятор">C501 - електронний терморегулятор</option>' +
        '<option value="C511T терморегулятор з таймером">C511T терморегулятор з таймером</option>' +
        '<option value="Comfort Eco - терморегулятор з таймером">Comfort Eco - терморегулятор з таймером</option>' +
        '<option value="Comfort Touch - сенсорний терморегулятор">Comfort Touch - сенсорний терморегулятор</option>' +
        '<option value="Comfort WiFi - терморегулятор">Comfort WiFi - терморегулятор</option>' +
      '</select>' +
    '</td>' +
    '<td class="calc-table-body-item">' +
      '<input type="text" class="input input-square">' +
    '</td>' +
    '<td class="calc-table-body-item">' +
      '<p class="calc-cost">13 562 грн</p>' +
    '</td>' +
  '</tr>');
});

$(document).on('click', '.js-remove-btn', function() {
  $(this).parents('.calc-table-body-row').remove();
});

$("a.scrollto").click(function() {
  var elementClick = $(this).attr("href")
  var destination = $(elementClick).offset().top - 100;
  jQuery("html:not(:animated),body:not(:animated)").animate({
    scrollTop: destination
  }, 800);
  return false;
});

$(window).bind('scroll', function() {
  var currentTop = $(window).scrollTop();
  var elems = $('.scrollspy');
  elems.each(function(index){
    var elemTop 	= $(this).offset().top - 100;
    var elemBottom 	= elemTop + $(this).height();
    if(currentTop >= elemTop && currentTop <= elemBottom){
      var id 		= $(this).attr('id');
      var navElem = $('a[href="#' + id+ '"]');
      $('.header-menu-link').removeClass('active')
  navElem.addClass('active');
    }
  })
}); 

$(document).on('click', '.call-menu', function(){
  $('.header-m-menu').addClass('active');
});
$(document).on('click', '.close-menu', function(){
  $('.header-m-menu').removeClass('active');
});

$(document).on('click', '.advantages-item-name', function(){
  $(this).siblings('.advantages-item-text').stop().animate().slideToggle();
});